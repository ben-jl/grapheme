# grapheme

A hobby version control system, with a data model (currently) similar to `git`'s,
backed by a sqlite database. Currently supports very basic get and put operations
for blobs (raw byte arrays) and trees (map of names to blobs/trees).

## Usage

### Init

```bash
$ cargo run --quiet -- init .test_contents/.test_store.db
Successfully initialized repository at .test_contents/.test_store.db
```

### Hash-File

```bash
$ cargo run --quiet -- hash-file LICENSE .test_contents/.test_store.db 
bbe4c9b990e1b513bb917247f240174b6d245fdcff665eccd0e3aedf4c7525e4
```

### Cat-File

```bash
$ cargo run --quiet -- cat-file bbe4c9b990e1b513bb917247f240174b6d245fdcff665eccd0e3aedf4c7525e4 .test_contents/.test_store.db 
BSD 2-Clause License

Copyright (c) 2021, Ben LeValley
All rights reserved.
...
```

### Checkout-Tree

```bash
$ cargo run -q -- checkout-tree 387d69a2c5dc20f0473c6b8812943c59aa5526d71531b5c6d25962f428668d07 .test_contents/test_checkout_2 .test_contents/.test_store_new 
Checkout out 387d69a2c5dc20f0473c6b8812943c59aa5526d71531b5c6d25962f428668d07 to .test_contents/test_checkout_2
  
$ ls -R .test_contents/test_checkout_2/ 
.test_contents/test_checkout_2/:
blob_2.txt  blob_3.txt  tree_1.txt
  
.test_contents/test_checkout_2/tree_1.txt:
blob_1.txt
```

### Make-Tree

```bash
$ cargo run -q make-tree --cref b980a1e393060b74010372e6b78a1697bad1786d7b37f0ff46d58476e968f429 Cargo.lock --cref b2af4ef552d245796d5763534e288a23a4526ae90f895e715ef6ee3a772c6b78 .rustc_info.json
c2e4b031a8efadd80d43744cd794406fb374dc25498f624179a6b789eb1228fb
```

### Create-Treeset

```bash
$ cargo run -q -- create-treeset 5acc0a678694145c4fc8e14fd1f06ee0f2618543904960641b528d24348aad90 202111121651 benlevalley "test message"
Created treeset 
fbec4e05738f23dfcb8dbdcea35318298a29ca64895bfa950f98eae04382d6fc

$ cargo run -q -- create-treeset --parent fbec4e05738f23dfcb8dbdcea35318298a29ca64895bfa950f98eae04382d6fc 5acc0a678694145c4fc8e14fd1f06ee0f2618543904960641b528d24348aad90 202111121652 benlevalley "test update"
Created treeset
a52b7e0e63ca7080479504fc48c2f94408432a2b122b4e36fd1145bf8891ccd7
```
