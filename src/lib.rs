use std::{path::PathBuf, str::FromStr};

mod checkout_treeset;
mod create_tree;
mod create_treeset;
mod list_trees;

pub use checkout_treeset::{checkout_treeset, CheckoutTreesetCommand};
pub use create_tree::{create_tree, CreateTreeCommand};
pub use create_treeset::{create_treeset, CreateTreesetCommand};
pub use list_trees::{list_trees, ListTreesCommand};

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct InitStoreCommand {
    store_path: String,
}

impl InitStoreCommand {
    pub fn store_path(&self) -> &str {
        self.store_path.as_str()
    }

    pub fn new<K: ToString>(store_path: K) -> InitStoreCommand {
        let store_path = store_path.to_string();
        InitStoreCommand { store_path }
    }
}

pub fn initialize_store(init_command: InitStoreCommand) -> grapheme_db::Result<PathBuf> {
    let _repo = grapheme_db::Repo::open_or_create(init_command.store_path())?;
    let pb = PathBuf::from_str(init_command.store_path()).unwrap();
    Ok(pb)
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct HashFileCommand {
    store_path: String,
    file_path: String,
}

impl HashFileCommand {
    pub fn new<K, T>(store_path: K, file_path: T) -> HashFileCommand
    where
        K: ToString,
        T: ToString,
    {
        let store_path = store_path.to_string();
        let file_path = file_path.to_string();
        HashFileCommand {
            store_path,
            file_path,
        }
    }

    pub fn store_path(&self) -> &str {
        &self.store_path
    }

    pub fn file_path(&self) -> &str {
        &self.file_path
    }
}

pub fn hash_file(hash_file_command: HashFileCommand) -> grapheme_db::Result<String> {
    // let store = Store::new(hash_file_command.store_path())?;
    let fs = std::fs::read(hash_file_command.file_path()).expect("file path not found");
    let mut r = grapheme_db::Repo::open_or_create(hash_file_command.store_path())?;
    let bref = r.put_blob(fs)?;
    let id = bref.raw_id().as_string();
    Ok(id)
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct CatFileCommand {
    store_path: String,
    object_id: String,
}

impl CatFileCommand {
    pub fn new<K, T>(store_path: K, object_id: T) -> CatFileCommand
    where
        K: ToString,
        T: ToString,
    {
        let store_path = store_path.to_string();
        let object_id = object_id.to_string();
        CatFileCommand {
            store_path,
            object_id,
        }
    }

    pub fn store_path(&self) -> &str {
        &self.store_path
    }

    pub fn object_id(&self) -> &str {
        &self.object_id
    }
}

pub fn cat_file(cat_file_command: CatFileCommand) -> grapheme_db::Result<String> {
    let r = grapheme_db::Repo::open_or_create(cat_file_command.store_path())?;
    let oid = grapheme_db::ObjId::try_from(cat_file_command.object_id())?;
    let bref: grapheme_db::BlobRef = oid.into();
    let bs = r.get_blob(bref)?;

    let res = String::from_utf8(bs.clone().to_vec()).unwrap();
    Ok(res)
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct CheckoutTreeTo {
    tree_ref_id: String,
    checkout_location: String,
    store_path: String,
}

impl CheckoutTreeTo {
    pub fn new<K, T, L>(store_path: L, tree_ref_id: T, checkout_location: K) -> CheckoutTreeTo
    where
        K: ToString,
        T: ToString,
        L: ToString,
    {
        let tree_ref_id = tree_ref_id.to_string();
        let checkout_location = checkout_location.to_string();
        let store_path = store_path.to_string();
        CheckoutTreeTo {
            store_path,
            tree_ref_id,
            checkout_location,
        }
    }
}

pub fn checkout_tree(checkout_tree_to: CheckoutTreeTo) -> grapheme_db::Result<()> {
    let r = grapheme_db::Repo::open_or_create(checkout_tree_to.store_path)?;
    let oid = grapheme_db::ObjId::try_from(checkout_tree_to.tree_ref_id)?;
    let tref: grapheme_db::TreeRef = oid.into();
    r.walk_tree(tref)
        .checkout_to(checkout_tree_to.checkout_location)
}
