use std::process::exit;

use clap::{App, Arg, SubCommand};
use grapheme::{
    cat_file, checkout_tree, checkout_treeset, create_tree, create_treeset, hash_file,
    CatFileCommand, CheckoutTreeTo, CheckoutTreesetCommand, CreateTreeCommand,
    CreateTreesetCommand, HashFileCommand, InitStoreCommand, ListTreesCommand,
};

pub fn main() -> std::io::Result<()> {
    let matches = App::new("grph")
        .author("Ben LeValley")
        .version("0.0.1")
        .subcommand(
            SubCommand::with_name("init")
                .help("Initializes a store at PATH")
                .arg(
                    Arg::with_name("PATH")
                        .help("PATH to location for grph db")
                        .takes_value(true)
                        .default_value(".grph.db"),
                ),
        )
        .subcommand(
            SubCommand::with_name("hash-file")
                .arg(Arg::with_name("FILE_PATH").takes_value(true).required(true))
                .arg(
                    Arg::with_name("STORE_PATH")
                        .takes_value(true)
                        .default_value(".grph.db"),
                ),
        )
        .subcommand(
            SubCommand::with_name("cat-file")
                .arg(Arg::with_name("BLOB_ID").takes_value(true).required(true))
                .arg(
                    Arg::with_name("STORE_PATH")
                        .takes_value(true)
                        .default_value(".grph.db"),
                ),
        )
        .subcommand(
            SubCommand::with_name("checkout-tree")
                .arg(Arg::with_name("TREE_ID").takes_value(true).required(true))
                .arg(
                    Arg::with_name("CHECKOUT_LOCATION")
                        .takes_value(true)
                        .default_value("."),
                )
                .arg(
                    Arg::with_name("STORE_PATH")
                        .takes_value(true)
                        .default_value(".grph.db"),
                ),
        )
        .subcommand(
            SubCommand::with_name("make-tree")
                .arg(
                    Arg::with_name("CREFS")
                        .long("cref")
                        .takes_value(true)
                        .multiple(true)
                        .min_values(1)
                        .value_names(&["REF_ID", "REF_NAME"]),
                )
                .arg(
                    Arg::with_name("STORE_PATH")
                        .takes_value(true)
                        .default_value(".grph.db"),
                ),
        )
        .subcommand(
            SubCommand::with_name("create-treeset")
                .arg(Arg::with_name("TREE_ID").takes_value(true).required(true))
                .arg(Arg::with_name("TIMESTAMP").takes_value(true).required(true))
                .arg(Arg::with_name("AUTHOR").takes_value(true).required(true))
                .arg(Arg::with_name("MESSAGE").takes_value(true).required(true))
                .arg(
                    Arg::with_name("STORE_PATH")
                        .takes_value(true)
                        .default_value(".grph.db"),
                )
                .arg(
                    Arg::with_name("PARENT_IDS")
                        .long("parent")
                        .takes_value(true),
                ),
        )
        .subcommand(
            SubCommand::with_name("list-trees").arg(
                Arg::with_name("STORE_PATH")
                    .takes_value(true)
                    .default_value(".grph.db"),
            ),
        )
        .subcommand(
            SubCommand::with_name("checkout-treeset")
                .arg(
                    Arg::with_name("TREESET_ID")
                        .takes_value(true)
                        .required(true),
                )
                .arg(
                    Arg::with_name("CHECKOUT_LOCATION")
                        .takes_value(true)
                        .default_value("."),
                )
                .arg(
                    Arg::with_name("STORE_PATH")
                        .takes_value(true)
                        .default_value(".grph.db"),
                ),
        )
        .get_matches();

    match matches.subcommand() {
        ("list-trees", Some(args)) => {
            let path = args
                .value_of("STORE_PATH")
                .expect("cannot init store w/out path");
            let comm = ListTreesCommand::new(path);
            grapheme::list_trees(comm)?;
        }
        ("init", Some(args)) => {
            let path = args.value_of("PATH").expect("cannot init store w/out path");
            let path: String = path.to_string();
            let command = InitStoreCommand::new(path);
            let store_path = grapheme::initialize_store(command)?;
            eprintln!(
                "Successfully initialized repository at {}",
                store_path.as_os_str().to_str().unwrap()
            );
        }
        ("hash-file", Some(args)) => {
            let store_path = args
                .value_of("STORE_PATH")
                .expect("cannot init store w/out path");
            let store_path: String = store_path.to_string();
            let file_path: &str = args.value_of("FILE_PATH").expect("CANNOT FIND file path");
            let file_path = file_path.to_string();
            let command = HashFileCommand::new(store_path, file_path);
            let file_sha = hash_file(command)?;
            eprintln!("{}", file_sha);
        }
        ("cat-file", Some(ctc)) => {
            let store_path = ctc
                .value_of("STORE_PATH")
                .expect("cannot init store w/out path");
            let store_path: String = store_path.to_string();

            let obj_id = ctc.value_of("BLOB_ID").expect("Failed");
            let command = CatFileCommand::new(store_path, obj_id);
            let result = cat_file(command)?;
            eprintln!("{}", result);
        }
        ("checkout-tree", Some(chc)) => {
            let store_path = chc
                .value_of("STORE_PATH")
                .expect("cannot init store w/out path");
            let store_path: String = store_path.to_string();

            let obj_id = chc.value_of("TREE_ID").expect("Failed");
            let checkout_loc = chc.value_of("CHECKOUT_LOCATION").expect("failed");
            let comm = CheckoutTreeTo::new(store_path, obj_id, checkout_loc);
            checkout_tree(comm)?;
            eprintln!("Checkout out {} to {}", obj_id, checkout_loc);
        }
        ("make-tree", Some(mkt)) => {
            let store_path = mkt
                .value_of("STORE_PATH")
                .expect("cannot init store w/out path");
            let store_path: String = store_path.to_string();
            let refnames = mkt
                .values_of("CREFS")
                .expect("must have at least one cref")
                .collect::<Vec<_>>()
                .chunks(2)
                .map(|rname| (rname[1].to_string(), rname[0].to_string()))
                .collect::<Vec<(String, String)>>();

            let comm = CreateTreeCommand::new(store_path, refnames);
            create_tree(comm)?;
        }
        ("create-treeset", Some(cts)) => {
            let store_path = cts
                .value_of("STORE_PATH")
                .expect("cannot init store w/out path");
            let author = cts.value_of("AUTHOR").expect("author not found");
            let timestamp = cts.value_of("TIMESTAMP").expect("timestamp not found");
            let message = cts.value_of("MESSAGE").expect("message not found");
            let tree_id = cts.value_of("TREE_ID").expect("tree_id not found");
            let parents: Vec<&str> = cts
                .value_of("PARENT_IDS")
                .map(|e| vec![e])
                .unwrap_or(vec![]);
            let comm =
                CreateTreesetCommand::new(store_path, timestamp, tree_id, message, author, parents);
            let ts = create_treeset(comm)?;
            eprintln!("Created treeset\n{}", ts);
        }
        ("checkout-treeset", Some(chc)) => {
            let store_path = chc
                .value_of("STORE_PATH")
                .expect("cannot init store w/out path");
            let store_path: String = store_path.to_string();

            let obj_id = chc.value_of("TREESET_ID").expect("Failed");
            let checkout_loc = chc.value_of("CHECKOUT_LOCATION").expect("failed");
            let comm = CheckoutTreesetCommand::new(store_path, obj_id, checkout_loc);
            checkout_treeset(comm)?;
            eprintln!("Checkout out {} to {}", obj_id, checkout_loc);
        }
        _ => {
            eprintln!("Unexpected command");
            exit(-1);
        }
    }
    Ok(())
}
