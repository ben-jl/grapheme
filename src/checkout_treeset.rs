use grapheme_db::Repo;

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct CheckoutTreesetCommand {
    store_path: String,
    treeset_id: String,
    checkout_location: String,
}

impl CheckoutTreesetCommand {
    pub fn new<K: ToString, L: ToString, Z: ToString>(
        store_path: K,
        treeset_id: L,
        checkout_location: Z,
    ) -> CheckoutTreesetCommand {
        CheckoutTreesetCommand {
            store_path: store_path.to_string(),
            treeset_id: treeset_id.to_string(),
            checkout_location: checkout_location.to_string(),
        }
    }
}

pub fn checkout_treeset(command: CheckoutTreesetCommand) -> grapheme_db::Result<()> {
    let r = Repo::open_or_create(command.store_path)?;
    let treeset = r.get_treeset(command.treeset_id)?;
    let tref = treeset.tree_ref();

    r.walk_tree(tref.clone())
        .checkout_to(command.checkout_location)?;
    Ok(())
}
