use grapheme_db::{GraphemeDbError, Repo};

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct CreateTreeCommand {
    store_path: String,
    direct_child_ids: Vec<(String, String)>,
}

impl CreateTreeCommand {
    pub fn new<K: ToString, T: IntoIterator<Item = (K, K)>>(
        store_path: K,
        child_ids: T,
    ) -> CreateTreeCommand {
        let store_path = store_path.to_string();
        let child_ids: Vec<(String, String)> = child_ids
            .into_iter()
            .map(|(e, n)| (e.to_string(), n.to_string()))
            .collect();
        CreateTreeCommand {
            store_path,
            direct_child_ids: child_ids,
        }
    }
}

pub fn create_tree(create_tree_command: CreateTreeCommand) -> grapheme_db::Result<()> {
    let r = Repo::open_or_create(create_tree_command.store_path)?;
    let mut crefs = vec![];
    create_tree_command
        .direct_child_ids
        .iter()
        .try_for_each(|(name, f)| {
            let resoved = r
                .resolve(f.to_string())
                .ok_or(GraphemeDbError::new("blob not found"))?;
            crefs.push((name, resoved));
            let result: Result<(), GraphemeDbError> = Ok(());
            result
        })?;
    let tid = r.create_tree(crefs)?;
    eprintln!("{}", tid.raw_id().as_string());
    Ok(())
}
