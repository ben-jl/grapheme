use grapheme_db::Repo;

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct CreateTreesetCommand {
    store_path: String,
    tree_id: String,
    message: String,
    author: String,
    timestamp: String,
    parents: Vec<String>,
}

impl CreateTreesetCommand {
    pub fn new<S, T, M, A, L, P>(
        store_path: S,
        timestamp: L,
        tree_id: T,
        message: M,
        author: A,
        parents: Vec<P>,
    ) -> CreateTreesetCommand
    where
        S: ToString,
        T: ToString,
        M: ToString,
        A: ToString,
        L: ToString,
        P: ToString,
    {
        let store_path = store_path.to_string();
        let tree_id = tree_id.to_string();
        let message = message.to_string();
        let author = author.to_string();
        let timestamp = timestamp.to_string();

        let parents = parents
            .into_iter()
            .map(|e| e.to_string())
            .collect::<Vec<String>>();
        CreateTreesetCommand {
            store_path,
            tree_id,
            message,
            author,
            timestamp,
            parents,
        }
    }
}

pub fn create_treeset(create_treeset_command: CreateTreesetCommand) -> grapheme_db::Result<String> {
    let mut r = Repo::open_or_create(create_treeset_command.store_path)?;
    let tree_id = grapheme_db::ObjId::try_from(create_treeset_command.tree_id)?;
    let mut cparents: Vec<grapheme_db::ObjId> = Vec::new();
    for p in create_treeset_command.parents.iter() {
        let pid = grapheme_db::ObjId::try_from(p.to_string())?;
        cparents.push(pid);
    }

    let tsetref = r.make_treeset(
        tree_id.into(),
        create_treeset_command.timestamp,
        create_treeset_command.author,
        create_treeset_command.message,
        cparents,
    )?;
    Ok(tsetref.raw_id().as_string())
}
