use grapheme_db::Repo;

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct ListTreesCommand {
    store_path: String,
}

impl ListTreesCommand {
    pub fn new<K: ToString>(store_path: K) -> ListTreesCommand {
        ListTreesCommand {
            store_path: store_path.to_string(),
        }
    }
}

pub fn list_trees(command: ListTreesCommand) -> grapheme_db::Result<()> {
    let r = Repo::open_or_create(command.store_path)?;
    let trees = r.get_trees()?;
    for tree in trees {
        println!("{}", tree.raw_id().as_string());
    }
    Ok(())
}
