use std::borrow::BorrowMut;

use criterion::{black_box, criterion_group, criterion_main, Criterion};
use grapheme_db::Repo;

fn insert_single_byte_blob(repo: &mut Repo, blob: u8) -> () {
    let _oid = repo.put_blob(vec![blob]).expect("failed to put blob");
}

pub fn benchmark_insert_single_byte_blob_to_empty_db(c: &mut Criterion) {
    let mut r = Repo::new_in_memory_repo().expect("failed to init repo");
    c.bench_function("benchmark_insert_single_byte_blob_to_empty_db", |b| {
        b.iter(|| insert_single_byte_blob(r.borrow_mut(), black_box(255)))
    });
}

criterion_group!(
    insert_benches,
    benchmark_insert_single_byte_blob_to_empty_db
);
criterion_main!(insert_benches);
