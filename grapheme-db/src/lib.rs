use sha2::Digest;

mod objid;
mod repo;
mod tree_walker;

pub use objid::{BlobRef, ObjId, TreeRef, TreesetRef};
pub use repo::Repo;

pub type Result<T> = std::result::Result<T, GraphemeDbError>;

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord)]
pub struct GraphemeDbError {
    message: String,
}

impl GraphemeDbError {
    pub fn new<K: ToString>(message: K) -> GraphemeDbError {
        let message = message.to_string();
        GraphemeDbError { message }
    }

    pub fn message(&self) -> &str {
        self.message.as_str()
    }
}

impl std::fmt::Display for GraphemeDbError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.message())
    }
}

impl From<rusqlite::Error> for GraphemeDbError {
    fn from(e: rusqlite::Error) -> Self {
        GraphemeDbError::new(e.to_string())
    }
}

impl From<GraphemeDbError> for std::io::Error {
    fn from(e: GraphemeDbError) -> Self {
        std::io::Error::new(std::io::ErrorKind::Other, e.message())
    }
}

fn hash_raw(bytes: &[u8]) -> Result<ObjId> {
    let mut h = sha2::Sha256::new();
    h.update(bytes);
    let res = h.finalize();
    let id = hex::encode(res);
    ObjId::try_from(id)
}
