use std::path::Path;

use crate::{
    hash_raw,
    objid::{BlobRef, GrRef, ObjId, RefName, TreeRef, TreesetRef},
    tree_walker::ConcreteTreeWelker,
    GraphemeDbError, Result,
};

use rusqlite::{params, Connection};
use sha2::{Digest, Sha256};
pub struct Repo {
    connection: Connection,
}

impl Repo {
    pub fn new_in_memory_repo() -> Result<Self> {
        let connection = Connection::open_in_memory()?;
        init_tables(&connection)?;
        Ok(Repo { connection })
    }

    pub fn open_or_create<P: AsRef<Path>>(path: P) -> Result<Self> {
        let path: &Path = path.as_ref();
        let connection = Connection::open(path)?;
        init_tables(&connection)?;
        Ok(Repo { connection })
    }

    fn connection(&self) -> &Connection {
        &self.connection
    }

    pub fn put_blob<T: AsRef<[u8]>>(&mut self, bytes: T) -> Result<BlobRef> {
        let bytes: &[u8] = bytes.as_ref();
        let id = hash_raw(bytes)?;

        self.connection().execute(
            "INSERT OR IGNORE INTO blobs (id, bytes) VALUES (?1, ?2);",
            params![id.as_string(), bytes],
        )?;
        Ok(id.into())
    }

    pub fn get_blob<T>(&self, bref: T) -> Result<Vec<u8>>
    where
        T: Into<BlobRef>,
    {
        let bid: BlobRef = bref.into();
        self.connection()
            .query_row(
                "SELECT bytes FROM blobs WHERE id = ?1",
                params![bid.raw_id().as_string()],
                |row| row.get(0),
            )
            .or(Err(GraphemeDbError::new(format!(
                "No blob found with id {}",
                bid.raw_id().as_string()
            ))))
    }

    pub fn get_tree<T>(&self, tref: T) -> Result<Vec<RefName>>
    where
        T: Into<TreeRef>,
    {
        let tref: TreeRef = tref.into();
        let tree_id: String = self.connection().query_row(
            "SELECT id FROM trees WHERE id = ?1",
            params![tref.raw_id().as_string()],
            |row| row.get(0),
        )?;

        let mut bgetstmt = self
            .connection()
            .prepare("SELECT blob_id, name FROM tree_blob_maps WHERE tree_id = ?1;")?;
        let brows = bgetstmt.query_map(params![&tree_id], |row| {
            let child_blob_id: String = row.get(0)?;
            let child_blob_name: String = row.get(1)?;
            let oid: ObjId =
                ObjId::try_from(child_blob_id).map_err(|_e| rusqlite::Error::InvalidQuery)?;
            Ok((GrRef::BlobRef(BlobRef::from(oid.into())), child_blob_name))
        })?;

        let mut tgetstmt = self
            .connection()
            .prepare("SELECT child_tree_id, name from tree_tree_maps WHERE tree_id = ?1;")?;
        let trows = tgetstmt.query_map(params![&tree_id], |row| {
            let child_tree_id: String = row.get(0)?;
            let child_tree_name: String = row.get(1)?;
            let oid: ObjId =
                ObjId::try_from(child_tree_id).map_err(|_e| rusqlite::Error::InvalidQuery)?;

            Ok((GrRef::TreeRef(TreeRef::from(oid.into())), child_tree_name))
        })?;

        let rows: Vec<RefName> = brows
            .filter_map(|el| el.ok())
            .chain(trows.filter_map(|e| e.ok()))
            .map(|e| e.into())
            .collect();

        Ok(rows)
    }

    pub fn create_tree<T, K, J>(&self, refs: T) -> Result<TreeRef>
    where
        T: IntoIterator<Item = (J, K)>,
        K: Into<GrRef> + Clone,
        J: AsRef<str>,
    {
        let mut hasher = Sha256::new();
        let mut processed_refs = vec![];
        let refs: Vec<(J, K)> = refs.into_iter().collect();
        for (ref sname, ref gref) in refs.iter() {
            processed_refs.push((
                format!(
                    "{} {}",
                    sname.as_ref(),
                    gref.clone().into().raw_id().as_string()
                ),
                sname.clone(),
                gref.clone(),
            ));
        }

        processed_refs.sort_by(|(ref name1, _, _), (ref name2, _, _)| name1.cmp(name2));
        processed_refs
            .iter()
            .try_for_each::<_, std::result::Result<(), GraphemeDbError>>(|(el, _, _)| {
                hasher.update(el.as_bytes());
                Ok(())
            })?;

        let tree_id: Vec<u8> = hasher.finalize().to_vec();
        let tree_id = hex::encode(tree_id);
        let tree_id: ObjId = ObjId::try_from(tree_id)?;

        let mut stmt = self.connection().prepare(
            "
            INSERT OR IGNORE INTO trees (id) VALUES (?1);
        ",
        )?;
        stmt.execute(params![tree_id.as_string()])?;

        let mut tree_child_stmt = self.connection().prepare(
            "
        INSERT OR IGNORE INTO tree_tree_maps (tree_id, child_tree_id, name) 
        VALUES (?1, ?2, ?3);",
        )?;

        processed_refs
            .iter()
            .try_for_each::<_, std::result::Result<(), GraphemeDbError>>(|(_, name, gref)| {
                let _result = match GrRef::from(gref.clone().into()) {
                    GrRef::BlobRef(br) => self.connection().execute(
                        "
                    INSERT OR IGNORE INTO tree_blob_maps (blob_id, tree_id, name)
                    VALUES (?1, ?2, ?3);
                ",
                        params![br.raw_id().as_string(), tree_id.as_string(), name.as_ref()],
                    ),
                    GrRef::TreeRef(tr) => tree_child_stmt.execute(params![
                        tree_id.as_string(),
                        tr.raw_id().as_string(),
                        name.as_ref()
                    ]),
                    GrRef::TreesetRef(_) => unimplemented!("treeset ref in walk"),
                }?;
                Ok(())
            })?;
        Ok(TreeRef::from(tree_id.into()).into())
    }

    pub fn walk_tree<T>(&self, starting_ref: T) -> ConcreteTreeWelker
    where
        T: Into<TreeRef>,
    {
        ConcreteTreeWelker::new(starting_ref, self)
    }

    pub fn resolve<T>(&self, obj_id: T) -> Option<GrRef>
    where
        T: TryInto<ObjId>,
    {
        let oid: Option<ObjId> = obj_id.try_into().ok();
        if oid.is_none() {
            None
        } else {
            let oid = oid.unwrap();
            let bid = self.get_blob(oid).ok();
            if bid.is_some() {
                let bref: BlobRef = oid.into();
                Some(bref.into())
            } else if let Some(_) = self.get_tree(oid).ok() {
                let tref: TreeRef = oid.into();
                Some(tref.into())
            } else {
                None
            }
        }
    }

    pub fn make_treeset<K, L, M, T>(
        &mut self,
        tree_ref: T,
        timestamp: K,
        author: L,
        message: M,
        parents: Vec<T>,
    ) -> Result<TreesetRef>
    where
        K: ToString,
        L: ToString,
        M: ToString,
        T: TryInto<ObjId>,
    {
        let tref: ObjId = tree_ref
            .try_into()
            .map_err(|_e| GraphemeDbError::new("tree ref not valid object id"))?;
        let timestamp = timestamp.to_string();
        let author = author.to_string();
        let message = message.to_string();

        let mut cparents: Vec<ObjId> = Vec::new();
        for p in parents {
            let pid = p
                .try_into()
                .map_err(|_e| GraphemeDbError::new("tset parent ref not valid object id"))?;

            cparents.push(pid);
        }

        cparents.sort();
        let mut hasher = Sha256::new();
        hasher.update(tref.raw());
        hasher.update(timestamp.as_bytes());
        hasher.update(author.as_bytes());
        for cpar in cparents.iter() {
            hasher.update(cpar.raw());
        }
        hasher.update(message.as_bytes());

        let result = hex::encode(hasher.finalize());
        let tsetref: ObjId = result
            .try_into()
            .map_err(|_e| GraphemeDbError::new("tset ref not valid object id"))?;

        let mut stmt = self.connection().prepare(
            "
                INSERT OR IGNORE INTO treesets (id, tree_id, timestamp, author, message) 
                VALUES (?1, ?2, ?3, ?4, ?5);
             
            ",
        )?;

        stmt.execute(params![
            tsetref.as_string(),
            tref.as_string(),
            timestamp,
            author,
            message
        ])?;

        let mut pinsert = self.connection().prepare(
            "INSERT OR IGNORE INTO treesets_parent_map (treeset_id, parent_treeset_id)
        VALUES (?1, ?2);",
        )?;

        for cpar in cparents.iter() {
            pinsert
                .execute(params![tsetref.as_string(), cpar.as_string()])
                .map_err(|_e| {
                    GraphemeDbError::new(format!(
                        "Error inserting ({}, {}) into treesets_parent_map",
                        tsetref.as_string(),
                        cpar.as_string()
                    ))
                })?;
        }

        Ok(TreesetRef::new(
            tsetref,
            timestamp,
            tref.into(),
            message,
            author,
            cparents,
        ))
    }

    pub fn get_treeset<T>(&self, tsetref: T) -> Result<TreesetRef>
    where
        T: TryInto<ObjId>,
    {
        let tsetoid: ObjId = tsetref
            .try_into()
            .map_err(|_e| GraphemeDbError::new("tree ref not valid object id"))?;
        let tsetref: TreesetRef = self.connection().query_row(
            "SELECT id, tree_id, timestamp, author, message FROM treesets WHERE id = ?1",
            params![tsetoid.as_string()],
            |row| {
                let id: String = row.get(0)?;
                let id: ObjId = id.try_into().expect("bad obj id");
                let tree_id: String = row.get(1)?;
                let tree_id: ObjId = tree_id.try_into().expect("bad tree_id id");
                let timestamp: String = row.get(2)?;
                let author: String = row.get(3)?;
                let message: String = row.get(4)?;

                let mut pgetstmt = self.connection().prepare(
                    "SELECT parent_treeset_id FROM treesets_parent_map WHERE treeset_id = ?1;",
                )?;
                let parentrows = pgetstmt.query_map(params![id.as_string()], |row| {
                    let parent_treeset_id: String = row.get(0)?;
                    let oid: ObjId = ObjId::try_from(parent_treeset_id)
                        .map_err(|_e| rusqlite::Error::InvalidQuery)?;
                    Ok(oid)
                })?;
                let prows = parentrows
                    .into_iter()
                    .map(|e| e.unwrap())
                    .collect::<Vec<ObjId>>();

                Ok(TreesetRef::new(
                    id,
                    timestamp,
                    tree_id.into(),
                    message,
                    author,
                    prows,
                ))
            },
        )?;

        Ok(tsetref)
    }

    pub fn get_trees(&self) -> Result<Vec<TreeRef>> {
        let mut tquery = self.connection().prepare("SELECT id FROM trees;")?;
        let shas = tquery.query_map([], |row| row.get(0)).unwrap();
        let mut refs: Vec<TreeRef> = vec![];
        for s in shas {
            let s: String = s.unwrap();
            let oid: ObjId = s.try_into().expect("failed obj id conversion");
            refs.push(oid.into());
        }
        Ok(refs)
    }
}

fn init_tables(conn: &Connection) -> Result<()> {
    conn.execute_batch(
        "
        BEGIN;
        CREATE TABLE IF NOT EXISTS blobs (
            id VARCHAR(64) PRIMARY KEY,
            bytes BLOB
        );
        CREATE TABLE IF NOT EXISTS trees (
            id VARCHAR(64) PRIMARY KEY
        );
        CREATE TABLE IF NOT EXISTS tree_blob_maps (
            blob_id VARCHAR(64) NOT NULL REFERENCES blobs(id),
            tree_id VARCHAR(64) NOT NULL REFERENCES trees(id),
            name VARCHAR(2048) NOT NULL CHECK(name <> ''),
            UNIQUE(blob_id, tree_id, name)
        );
        CREATE TABLE IF NOT EXISTS tree_tree_maps (
            tree_id VARCHAR(64) NOT NULL REFERENCES trees(id),
            child_tree_id VARCHAR(64) NOT NULL REFERENCES trees(id),
            name VARCHAR(64) NOT NULL CHECK (name <> ''),
            CHECK (child_tree_id <> tree_id),
            UNIQUE(tree_id, child_tree_id, name)
        );
        CREATE TABLE IF NOT EXISTS treesets (
            id VARCHAR(64) PRIMARY KEY,
            tree_id VARCHAR(64) NOT NULL REFERENCES trees(id),
            timestamp VARCHAR(64) NOT NULL,
            author VARCHAR(2048) NOT NULL,
            message TEXT NOT NULL,
            UNIQUE(tree_id, timestamp, author, message)
        );
        CREATE TABLE IF NOT EXISTS treesets_parent_map (
            treeset_id VARCHAR(64) NOT NULL REFERENCES treesets(id),
            parent_treeset_id VARCHAR(64) NOT NULL REFERENCES treesets(id)           
        );
        COMMIT;
            ",
    )?;
    Ok(())
}

#[cfg(test)]
mod test {
    use crate::objid::ObjId;

    use super::Repo;

    #[test]
    fn it_initializes_repository_cleanly() {
        let _ = Repo::new_in_memory_repo().expect("failed to initialize repo");
    }

    #[test]
    fn it_puts_simple_blob_in_repo_cleanly() {
        let mut r = Repo::new_in_memory_repo().expect("failed to init repo");
        let bid = r
            .put_blob("one two three four".as_bytes())
            .expect("failed to put blob");
        assert_eq!(
            "113f69ccf08f80a63beba2f49218eaa72a183f374065e6cb56ebe9bd68eb79cc",
            bid.raw_id().as_string()
        );
    }

    #[test]
    fn it_doesnt_error_on_duplicate_blobs() {
        let mut r = Repo::new_in_memory_repo().expect("failed to init repo");
        let bid = r
            .put_blob("one two three four".as_bytes())
            .expect("failed to put blob");
        let bid2 = r
            .put_blob(b"one two three four")
            .expect("failed to put second blob");

        assert_eq!(
            "113f69ccf08f80a63beba2f49218eaa72a183f374065e6cb56ebe9bd68eb79cc",
            bid.raw_id().as_string()
        );
        assert_eq!(bid, bid2);
    }

    #[test]
    fn it_retrieves_simple_blob() {
        let mut r = Repo::new_in_memory_repo().expect("failed to init repo");
        let bid = r
            .put_blob(b"one two three four")
            .expect("failed to put blob");

        let contents = r.get_blob(bid).expect("failed to retrieve blobs");
        assert_eq!(
            vec![
                111, 110, 101, 32, 116, 119, 111, 32, 116, 104, 114, 101, 101, 32, 102, 111, 117,
                114
            ],
            contents
        );

        let bid2 = r
            .put_blob(contents.clone())
            .expect("failed to re-insert blob");
        let contents2 = r.get_blob(bid2).expect("failed to retrieve 2nd blob");
        assert_eq!(contents, contents2);
    }

    #[test]
    fn it_errors_if_attempt_to_get_nonexistent_blob() {
        let r = Repo::new_in_memory_repo().expect("failed to init repo");
        let id: ObjId = "113f69ccf08f80a63beba2f49218eaa72a183f374065e6cb56ebe9bd68eb79cc"
            .try_into()
            .unwrap();
        let result = r.get_blob(id);
        result.expect_err("Expected error nonexistent blob");
    }

    #[test]
    fn it_creates_a_tree_with_single_blob_child() {
        let mut r = Repo::new_in_memory_repo().expect("failed to init repo");
        let bid = r.put_blob(b"one two three").expect("failed to put blob");
        let tree = r
            .create_tree(vec![("blob_1.txt", bid)])
            .expect("failed to put tree");
        assert_eq!(
            "8ad33e60e6f45987c3e63ba5c6f8da7989956f68caa2cb59e089affe9b8780da",
            tree.raw_id().as_string()
        );
    }

    #[test]
    fn it_creates_a_tree_with_multiple_child_blobs() {
        let mut r = Repo::new_in_memory_repo().expect("failed to init repo");
        let bid1 = r.put_blob(b"one two three").expect("failed to put blob");
        let bid2 = r
            .put_blob(b"one two three four")
            .expect("failed to put blob");
        let bid3 = r
            .put_blob(b"one two three four five")
            .expect("failed to put blob");

        let tree = r
            .create_tree(vec![
                ("blob_1.txt", bid1),
                ("blob_2.txt", bid2),
                ("blob_3.txt", bid3),
            ])
            .expect("failed to put tree");
        assert_eq!(
            "1c702f124bfac65884f7523af3038d266e6b349cfb00183350bf5bff7a9329dc",
            tree.raw_id().as_string()
        );
    }

    #[test]
    fn it_creates_a_tree_with_multiple_child_blobs_and_tree() {
        let mut r = Repo::new_in_memory_repo().expect("failed to init repo");
        let bid1 = r.put_blob(b"one two three").expect("failed to put blob");
        let tree1 = r
            .create_tree(vec![("blob_1.txt", bid1)])
            .expect("failed to create child tree");

        let bid2 = r
            .put_blob(b"one two three four")
            .expect("failed to put blob");
        let bid3 = r
            .put_blob(b"one two three four five")
            .expect("failed to put blob");

        let tree = r
            .create_tree(vec![
                ("tree_1.txt", crate::objid::GrRef::from(tree1.into())),
                ("blob_2.txt", bid2.into()),
                ("blob_3.txt", bid3.into()),
            ])
            .expect("failed to put tree");
        assert_eq!(
            "387d69a2c5dc20f0473c6b8812943c59aa5526d71531b5c6d25962f428668d07",
            tree.raw_id().as_string()
        );
    }

    #[test]
    fn it_retrieves_a_tree_with_multiple_child_blobs() {
        let mut r = Repo::new_in_memory_repo().expect("failed to init repo");
        let bid1 = r.put_blob(b"one two three").expect("failed to put blob");
        let bid2 = r
            .put_blob(b"one two three four")
            .expect("failed to put blob");
        let bid3 = r
            .put_blob(b"one two three four five")
            .expect("failed to put blob");

        let tree = r
            .create_tree(vec![
                ("blob_1.txt", bid1),
                ("blob_2.txt", bid2),
                ("blob_3.txt", bid3),
            ])
            .expect("failed to put tree");

        let result = r.get_tree(tree).expect("failed");
        assert_eq!(3, result.len());
    }

    #[test]
    fn it_retrieves_a_tree_with_multiple_child_blobs_and_tree() {
        let mut r = Repo::new_in_memory_repo().expect("failed to init repo");
        let bid1 = r.put_blob(b"one two three").expect("failed to put blob");
        let tree1 = r
            .create_tree(vec![("blob_1.txt", bid1)])
            .expect("failed to create child tree");

        let bid2 = r
            .put_blob(b"one two three four")
            .expect("failed to put blob");
        let bid3 = r
            .put_blob(b"one two three four five")
            .expect("failed to put blob");

        let tree = r
            .create_tree(vec![
                ("tree_1.txt", crate::objid::GrRef::from(tree1.into())),
                ("blob_2.txt", bid2.into()),
                ("blob_3.txt", bid3.into()),
            ])
            .expect("failed to put tree");

        let returned = r.get_tree(tree).expect("failed");
        assert_eq!(3, returned.len());
    }

    #[test]
    fn it_creates_a_treeset_with_no_parents() {
        let mut r = Repo::new_in_memory_repo().expect("failed to init repo");
        let bid1 = r.put_blob(b"one two three").expect("failed to put blob");
        let bid2 = r
            .put_blob(b"one two three four")
            .expect("failed to put blob");
        let bid3 = r
            .put_blob(b"one two three four five")
            .expect("failed to put blob");

        let tree = r
            .create_tree(vec![
                ("blob_1.txt", bid1),
                ("blob_2.txt", bid2),
                ("blob_3.txt", bid3),
            ])
            .expect("failed to put tree");
        assert_eq!(
            "1c702f124bfac65884f7523af3038d266e6b349cfb00183350bf5bff7a9329dc",
            tree.raw_id().as_string()
        );

        let ts = r
            .make_treeset(
                tree.raw_id(),
                "12345",
                "testauthor",
                "treeset example message",
                vec![],
            )
            .expect("failed to make treeset");
        assert_eq!(
            "080803ca7297da8945df1351e7d177a844e5ec9fc1f485fcf700cac642c47ab7",
            ts.raw_id().as_string()
        );
    }

    #[test]
    fn it_retrievess_a_treeset_with_no_parents() {
        let mut r = Repo::new_in_memory_repo().expect("failed to init repo");
        let bid1 = r.put_blob(b"one two three").expect("failed to put blob");
        let bid2 = r
            .put_blob(b"one two three four")
            .expect("failed to put blob");
        let bid3 = r
            .put_blob(b"one two three four five")
            .expect("failed to put blob");

        let tree = r
            .create_tree(vec![
                ("blob_1.txt", bid1),
                ("blob_2.txt", bid2),
                ("blob_3.txt", bid3),
            ])
            .expect("failed to put tree");
        assert_eq!(
            "1c702f124bfac65884f7523af3038d266e6b349cfb00183350bf5bff7a9329dc",
            tree.raw_id().as_string()
        );

        let ts = r
            .make_treeset(
                tree.raw_id(),
                "12345",
                "testauthor",
                "treeset example message",
                vec![],
            )
            .expect("failed to make treeset");
        assert_eq!(
            "080803ca7297da8945df1351e7d177a844e5ec9fc1f485fcf700cac642c47ab7",
            ts.raw_id().as_string()
        );

        let ts2 = r.get_treeset(ts.raw_id()).expect("failed ts2");
        assert_eq!(ts, ts2);
    }

    #[test]
    fn it_creates_a_treeset_with_one_parent() {
        let mut r = Repo::new_in_memory_repo().expect("failed to init repo");
        let bid1 = r.put_blob(b"one two three").expect("failed to put blob");
        let bid2 = r
            .put_blob(b"one two three four")
            .expect("failed to put blob");
        let bid3 = r
            .put_blob(b"one two three four five")
            .expect("failed to put blob");

        let tree = r
            .create_tree(vec![
                ("blob_1.txt", bid1),
                ("blob_2.txt", bid2),
                ("blob_3.txt", bid3),
            ])
            .expect("failed to put tree");
        assert_eq!(
            "1c702f124bfac65884f7523af3038d266e6b349cfb00183350bf5bff7a9329dc",
            tree.raw_id().as_string()
        );

        let ts = r
            .make_treeset(
                tree.raw_id(),
                "12345",
                "testauthor",
                "treeset example message",
                vec![],
            )
            .expect("failed to make treeset");
        assert_eq!(
            "080803ca7297da8945df1351e7d177a844e5ec9fc1f485fcf700cac642c47ab7",
            ts.raw_id().as_string()
        );

        let tsc = r
            .make_treeset(
                tree.raw_id(),
                "4567",
                "testauthor2",
                "made an edit",
                vec![ts.raw_id()],
            )
            .expect("failed desc create");

        assert_eq!(1, tsc.parents().len());
        assert_eq!(
            "080803ca7297da8945df1351e7d177a844e5ec9fc1f485fcf700cac642c47ab7",
            tsc.parents()[0].as_string()
        );
    }

    #[test]
    fn it_retrieves_a_treeset_with_one_parent() {
        let mut r = Repo::new_in_memory_repo().expect("failed to init repo");
        let bid1 = r.put_blob(b"one two three").expect("failed to put blob");
        let bid2 = r
            .put_blob(b"one two three four")
            .expect("failed to put blob");
        let bid3 = r
            .put_blob(b"one two three four five")
            .expect("failed to put blob");

        let tree = r
            .create_tree(vec![
                ("blob_1.txt", bid1),
                ("blob_2.txt", bid2),
                ("blob_3.txt", bid3),
            ])
            .expect("failed to put tree");
        assert_eq!(
            "1c702f124bfac65884f7523af3038d266e6b349cfb00183350bf5bff7a9329dc",
            tree.raw_id().as_string()
        );

        let ts = r
            .make_treeset(
                tree.raw_id(),
                "12345",
                "testauthor",
                "treeset example message",
                vec![],
            )
            .expect("failed to make treeset");
        assert_eq!(
            "080803ca7297da8945df1351e7d177a844e5ec9fc1f485fcf700cac642c47ab7",
            ts.raw_id().as_string()
        );

        let tsc = r
            .make_treeset(
                tree.raw_id(),
                "4567",
                "testauthor2",
                "made an edit",
                vec![ts.raw_id()],
            )
            .expect("failed desc create");

        assert_eq!(1, tsc.parents().len());
        assert_eq!(
            "080803ca7297da8945df1351e7d177a844e5ec9fc1f485fcf700cac642c47ab7",
            tsc.parents()[0].as_string()
        );

        let ts_retrieved = r.get_treeset(tsc.raw_id()).expect("failed ts2");
        assert_eq!(tsc, ts_retrieved);
    }

    #[test]
    fn it_creates_a_treeset_with_two_parent() {
        let mut r = Repo::new_in_memory_repo().expect("failed to init repo");
        let bid1 = r.put_blob(b"one two three").expect("failed to put blob");
        let bid2 = r
            .put_blob(b"one two three four")
            .expect("failed to put blob");
        let bid3 = r
            .put_blob(b"one two three four five")
            .expect("failed to put blob");

        let tree = r
            .create_tree(vec![
                ("blob_1.txt", bid1),
                ("blob_2.txt", bid2),
                ("blob_3.txt", bid3),
            ])
            .expect("failed to put tree");
        assert_eq!(
            "1c702f124bfac65884f7523af3038d266e6b349cfb00183350bf5bff7a9329dc",
            tree.raw_id().as_string()
        );

        let ts1 = r
            .make_treeset(
                tree.raw_id(),
                "12345",
                "testauthor",
                "treeset example message",
                vec![],
            )
            .expect("failed to make treeset");
        assert_eq!(
            "080803ca7297da8945df1351e7d177a844e5ec9fc1f485fcf700cac642c47ab7",
            ts1.raw_id().as_string()
        );

        let ts2 = r
            .make_treeset(
                tree.raw_id(),
                "12345678",
                "testauthor",
                "treeset example message",
                vec![],
            )
            .expect("failed to make treeset");
        assert_eq!(
            "7b5550d4cf2d9ae179a36f30e422d52b512dd60b9f20eea3da5fc7ff9b3051c8",
            ts2.raw_id().as_string()
        );

        let tsc = r
            .make_treeset(
                tree.raw_id(),
                "4567",
                "testauthor2",
                "made an edit",
                vec![ts1.raw_id(), ts2.raw_id()],
            )
            .expect("failed desc create");

        assert_eq!(2, tsc.parents().len());
        assert_eq!(
            "080803ca7297da8945df1351e7d177a844e5ec9fc1f485fcf700cac642c47ab7",
            tsc.parents()[0].as_string()
        );
        assert_eq!(
            "7b5550d4cf2d9ae179a36f30e422d52b512dd60b9f20eea3da5fc7ff9b3051c8",
            tsc.parents()[1].as_string()
        );
    }

    #[test]
    fn it_retrieves_a_treeset_with_two_parent() {
        let mut r = Repo::new_in_memory_repo().expect("failed to init repo");
        let bid1 = r.put_blob(b"one two three").expect("failed to put blob");
        let bid2 = r
            .put_blob(b"one two three four")
            .expect("failed to put blob");
        let bid3 = r
            .put_blob(b"one two three four five")
            .expect("failed to put blob");

        let tree = r
            .create_tree(vec![
                ("blob_1.txt", bid1),
                ("blob_2.txt", bid2),
                ("blob_3.txt", bid3),
            ])
            .expect("failed to put tree");

        let ts1 = r
            .make_treeset(
                tree.raw_id(),
                "12345",
                "testauthor",
                "treeset example message",
                vec![],
            )
            .expect("failed to make treeset");

        let ts2 = r
            .make_treeset(
                tree.raw_id(),
                "12345678",
                "testauthor",
                "treeset example message",
                vec![],
            )
            .expect("failed to make treeset");

        let tsc = r
            .make_treeset(
                tree.raw_id(),
                "4567",
                "testauthor2",
                "made an edit",
                vec![ts1.raw_id(), ts2.raw_id()],
            )
            .expect("failed desc create");

        let tsc2 = r.get_treeset(tsc.raw_id()).expect("failed tset get");
        assert_eq!(tsc, tsc2);
    }
}
