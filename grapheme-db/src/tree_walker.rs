use std::path::PathBuf;

use crate::{
    objid::{GrRef, RefName},
    Repo, Result, TreeRef,
};

pub struct ConcreteTreeWelker<'a> {
    in_progress: Vec<(Vec<RefName>, RefName)>,
    repo: &'a Repo,
    started_at: TreeRef,
}

impl<'a> ConcreteTreeWelker<'a> {
    pub fn started_at(&self) -> &TreeRef {
        &self.started_at
    }
}

impl<'a> ConcreteTreeWelker<'a> {
    pub fn new<P: Into<TreeRef>>(initial: P, repo: &'a Repo) -> Self {
        let initial: TreeRef = initial.into();
        let in_progress = vec![(vec![], (initial.into(), ".".to_string()).into())];
        ConcreteTreeWelker {
            started_at: initial,
            in_progress,
            repo,
        }
    }

    fn repo(&self) -> &'a Repo {
        &self.repo
    }

    pub fn walk<F>(&mut self, mut for_entity: F) -> ()
    where
        F: FnMut(TreeWalkRoute) -> bool,
    {
        while let Some((mut path, refname)) = self.in_progress.pop() {
            path.push(refname.clone());
            match &refname.gref() {
                GrRef::BlobRef(_br) => (),
                GrRef::TreeRef(tr) => {
                    let crefs = self
                        .repo
                        .get_tree(tr.clone())
                        .expect("failed to get known ref");

                    crefs.iter().for_each(|nxref| {
                        self.in_progress.push((path.clone(), nxref.clone()));
                    });
                    ()
                }
                GrRef::TreesetRef(_) => unimplemented!("treeset ref in walk"),
            };

            let twr = TreeWalkRoute::new(self.started_at().clone(), path, refname.clone());
            let should_continue = for_entity(twr);
            if !should_continue {
                return;
            }
        }
    }

    pub fn checkout_to<P>(&mut self, path: P) -> Result<()>
    where
        P: AsRef<str>,
    {
        let repo = self.repo();
        self.walk(|twr| {
            let path = path.as_ref().to_string();
            let full_path = twr.as_path(path);
            match twr.terminal().gref() {
                GrRef::BlobRef(br) => {
                    let bytes = repo.get_blob(br.clone()).expect("failed");
                    std::fs::write(full_path, bytes).expect("failed blob write");
                }
                GrRef::TreeRef(_tr) => {
                    std::fs::create_dir_all(full_path).expect("failed path create");
                }
                GrRef::TreesetRef(_) => unimplemented!("treeset ref in walk"),
            }

            true
        });
        Ok(())
    }
}

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct TreeWalkRoute {
    started_at: TreeRef,
    path: Vec<RefName>,
    terminal: RefName,
}

impl TreeWalkRoute {
    pub fn terminal(&self) -> &RefName {
        &self.terminal
    }

    pub fn path(&self) -> &[RefName] {
        &self.path
    }

    pub fn new(started_at: TreeRef, path: Vec<RefName>, terminal: RefName) -> TreeWalkRoute {
        TreeWalkRoute {
            path,
            terminal,
            started_at,
        }
    }

    pub fn started_at(&self) -> &TreeRef {
        &self.started_at
    }

    pub fn as_path<T: ToString>(&self, root: T) -> std::path::PathBuf {
        let root = root.to_string();
        let mut root = PathBuf::try_from(root).expect("failed");
        self.path()
            .iter()
            .skip(1)
            .for_each(|rn| root.push(rn.name().clone()));
        //root.push(self.terminal().name().clone());
        root
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn it_walks_single_level_tree_successfully() {
        let mut r = Repo::new_in_memory_repo().expect("failed repo init");
        let b1 = r.put_blob(b"one").expect("failed b1 put");
        let b2 = r.put_blob(b"one two").expect("failed b2 put");
        let b3 = r.put_blob(b"one two three").expect("failed b3 put");

        let t = r
            .create_tree(vec![("blob1", b1), ("blob2", b2), ("blob3", b3)])
            .expect("failed tree put");

        let mut allrefs: Vec<TreeWalkRoute> = Vec::new();
        r.walk_tree(t).walk(|twr: TreeWalkRoute| {
            allrefs.push(twr);
            true
        });

        assert_eq!(4, allrefs.len());
    }

    #[test]
    fn it_walks_multi_level_tree_successfully() {
        let mut r = Repo::new_in_memory_repo().expect("failed repo init");
        let b1 = r.put_blob(b"one").expect("failed b1 put");
        let b2 = r.put_blob(b"one two").expect("failed b2 put");
        let b3 = r.put_blob(b"one two three").expect("failed b3 put");

        let t1 = r
            .create_tree(vec![("blob1", b1), ("blob2", b2), ("blob3", b3)])
            .expect("failed tree put");

        let t2 = r
            .create_tree(vec![("child_tree", t1)])
            .expect("failed parent tree put");

        let mut allrefs: Vec<TreeWalkRoute> = Vec::new();
        r.walk_tree(t2).walk(|twr: TreeWalkRoute| {
            allrefs.push(twr);
            true
        });
        assert_eq!(5, allrefs.len());
    }

    #[test]
    fn it_walks_multi_level_tree_complicated_successfully() {
        let mut r = Repo::new_in_memory_repo().expect("failed repo init");
        let b1 = r.put_blob(b"one").expect("failed b1 put");
        let b2 = r.put_blob(b"one two").expect("failed b2 put");
        let b3 = r.put_blob(b"one two three").expect("failed b3 put");

        let t1 = r
            .create_tree(vec![("blob1", b1), ("blob2", b2), ("blob3", b3)])
            .expect("failed tree put");

        let b4 = r.put_blob(b"one two three four").expect("failed b4 put");
        let t2 = r
            .create_tree(vec![
                ("child_tree", GrRef::from(t1.into())),
                ("blob4.txt", GrRef::from(b4.into())),
            ])
            .expect("failed parent tree put");

        let mut allrefs: Vec<TreeWalkRoute> = Vec::new();
        r.walk_tree(t2).walk(|twr: TreeWalkRoute| {
            allrefs.push(twr);
            true
        });
        assert_eq!(6, allrefs.len());
    }

    #[test]
    fn it_returns_the_path_it_took_in_tree_walk_route() {
        let mut r = Repo::new_in_memory_repo().expect("failed repo init");
        let b = r.put_blob(b"one two three").expect("failed b1 put");
        let t1 = r
            .create_tree(vec![("blob.txt", b)])
            .expect("failed t1 tree create");
        let t2 = r
            .create_tree(vec![("tree1", t1)])
            .expect("failed t2 tree create");

        let mut allrefs: Vec<TreeWalkRoute> = Vec::new();
        r.walk_tree(t2).walk(|twr: TreeWalkRoute| {
            allrefs.push(twr);
            true
        });
        assert_eq!(3, allrefs.len())
    }
}
