use crate::GraphemeDbError;
use lazy_static::lazy_static;
use regex::Regex;

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct RefName {
    name: String,
    gref: GrRef,
}

impl RefName {
    pub fn name(&self) -> &str {
        &self.name
    }

    pub fn gref(&self) -> &GrRef {
        &self.gref
    }
}

impl Into<RefName> for (GrRef, String) {
    fn into(self) -> RefName {
        RefName {
            name: self.1,
            gref: self.0,
        }
    }
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum GrRef {
    BlobRef(BlobRef),
    TreeRef(TreeRef),
    TreesetRef(TreesetRef),
}

impl GrRef {
    pub fn ref_type(&self) -> &str {
        match self {
            &GrRef::BlobRef(_) => "blob",
            &GrRef::TreeRef(_) => "tree",
            &GrRef::TreesetRef(_) => "treeset",
        }
    }

    pub fn raw_id(&self) -> ObjId {
        match &self {
            GrRef::BlobRef(br) => br.raw_id().clone(),
            GrRef::TreeRef(tr) => tr.raw_id().clone(),
            GrRef::TreesetRef(trs) => trs.raw_id().clone(),
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct BlobRef(ObjId);

impl BlobRef {
    pub fn raw_id(&self) -> ObjId {
        self.0
    }
}

impl Into<BlobRef> for ObjId {
    fn into(self) -> BlobRef {
        BlobRef(self)
    }
}

impl Into<GrRef> for BlobRef {
    fn into(self) -> GrRef {
        GrRef::BlobRef(self)
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct TreeRef(ObjId);

impl TreeRef {
    pub fn raw_id(&self) -> ObjId {
        self.0
    }
}

impl Into<TreeRef> for ObjId {
    fn into(self) -> TreeRef {
        TreeRef(self)
    }
}

impl Into<GrRef> for TreeRef {
    fn into(self) -> GrRef {
        GrRef::TreeRef(self.into())
    }
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct TreesetRef {
    id: ObjId,
    timestamp: String,
    tree_ref: TreeRef,
    message: String,
    author: String,
    parents: Vec<ObjId>,
}

impl TreesetRef {
    pub fn new<K, T, L>(
        id: ObjId,
        timestamp: L,
        tree_ref: TreeRef,
        message: K,
        author: T,
        parents: Vec<ObjId>,
    ) -> TreesetRef
    where
        K: ToString,
        T: ToString,
        L: ToString,
    {
        let timestamp = timestamp.to_string();
        let message = message.to_string();
        let author = author.to_string();

        TreesetRef {
            id,
            timestamp,
            tree_ref,
            message,
            author,
            parents,
        }
    }

    pub fn raw_id(&self) -> ObjId {
        self.id
    }

    pub fn timestamp(&self) -> &str {
        &self.timestamp
    }

    pub fn tree_ref(&self) -> &TreeRef {
        &self.tree_ref
    }

    pub fn message(&self) -> &str {
        &self.message
    }

    pub fn author(&self) -> &str {
        &self.author
    }

    pub fn parents(&self) -> &[ObjId] {
        &self.parents
    }

    pub fn pretty(&self) -> String {
        let mut string = format!(
            "{} {} ({})",
            self.raw_id().as_string(),
            self.timestamp(),
            self.author()
        );
        string
            .push_str(format!("\nREFERS TO -> {}", self.tree_ref().raw_id().as_string()).as_str());
        for p in self.parents() {
            string.push_str(format!("\nDESC FROM -> {}", p.as_string()).as_str());
        }
        string.push_str("\n");
        string.push_str(self.message());
        string
    }
}

#[derive(Debug, Clone, Copy, PartialEq, PartialOrd, Eq, Ord, Hash)]
pub struct ObjId {
    raw_bytes: [u8; 64],
}

impl ObjId {
    pub fn as_string(&self) -> String {
        let id = String::from_utf8_lossy(self.raw()).to_string();
        id
    }

    fn from_raw(raw_bytes: [u8; 64]) -> ObjId {
        ObjId { raw_bytes }
    }

    pub fn raw(&self) -> &[u8] {
        &self.raw_bytes
    }
}

impl TryFrom<&str> for ObjId {
    type Error = GraphemeDbError;

    fn try_from(value: &str) -> Result<Self, Self::Error> {
        if !match_objid(value) {
            return Err(GraphemeDbError::new(
                "Expect object id to contain only alphanumeric chars",
            ));
        }
        let raw_bytes = string_to_byte_array(value)?;

        Ok(ObjId::from_raw(raw_bytes))
    }
}

impl TryFrom<String> for ObjId {
    type Error = GraphemeDbError;

    fn try_from(value: String) -> Result<Self, Self::Error> {
        if !match_objid(&value) {
            return Err(GraphemeDbError::new(
                "Expect object id to contain only alphanumeric chars",
            ));
        }
        let raw_bytes: [u8; 64] = string_to_byte_array(&value)?;

        Ok(ObjId::from_raw(raw_bytes))
    }
}

impl TryFrom<[u8; 64]> for ObjId {
    type Error = GraphemeDbError;

    fn try_from(value: [u8; 64]) -> Result<Self, Self::Error> {
        let id = value
            .iter()
            .fold("".to_string(), |acc, nxt| acc + &nxt.to_string());
        if !match_objid(&id) {
            return Err(GraphemeDbError::new(
                "Expect object id to contain only alphanumeric chars",
            ));
        }

        Ok(ObjId::from_raw(value))
    }
}

fn string_to_byte_array<T: AsRef<str>>(candidate: T) -> crate::Result<[u8; 64]> {
    let raw_bytes: [u8; 64] = candidate
        .as_ref()
        .as_bytes()
        .try_into()
        .map_err(|e| GraphemeDbError::new(&e))?;

    Ok(raw_bytes)
}

fn match_objid(candidate: &str) -> bool {
    lazy_static! {
        static ref ID_RE: Regex = Regex::new("[a-zA-Z0-9]{64}").unwrap();
    }

    candidate.len() == 64 && ID_RE.is_match(candidate)
}

#[cfg(test)]
mod test {
    use super::ObjId;

    #[test]
    fn it_is_ok_when_given_string_in_valid_obj_id_format() {
        let o: Result<ObjId, _> =
            "a54645e52896a033a1a8c88127c23ed5d988e01e096e99c295a160152d7dcde8"
                .to_string()
                .try_into();

        assert!(o.is_ok());

        let oid: ObjId = o.unwrap();

        assert_eq!(
            "a54645e52896a033a1a8c88127c23ed5d988e01e096e99c295a160152d7dcde8",
            oid.as_string()
        );
    }

    #[test]
    fn it_is_err_when_given_invalid_obj_ids() {
        let too_short: Result<ObjId, _> =
            "a54645e52896a033a1a8c88127c23ed5d988e01e096e99c295a160152d7dcde"
                .to_string()
                .try_into();
        let too_long: Result<ObjId, _> =
            "a54645e52896a033a1a8c88127c23ed5d988e01e096e99c295a160152d7dcdeaaa"
                .to_string()
                .try_into();
        let inv_char: Result<ObjId, _> =
            "a_4645e52896a033a1a8c88127c23ed5d988e01e096e99c295a160152d7dcde8"
                .to_string()
                .try_into();

        assert!(too_short.is_err());
        assert!(too_long.is_err());
        assert!(inv_char.is_err());
    }
}
